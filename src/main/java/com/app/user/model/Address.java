package com.app.user.model;

import lombok.Data;

@Data
public class Address {
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String addressLine4;
	private String locality; // area near by 
	private String region; // 
	private String city ;
	private String state;
	private String Country;
	private String postalCode;
}
