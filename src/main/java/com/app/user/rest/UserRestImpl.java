package com.app.user.rest;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.user.model.User;
import com.app.user.service.IUserService;

@RestController
@RequestMapping("/user")
public class UserRestImpl {
	
	private Logger logger = LogManager.getLogger(UserRestImpl.class);
	
	@Autowired
	private IUserService userService;
	
	@PostMapping("/save")
	public ResponseEntity<User> createNewUser (@RequestBody User user){
		logger.info("Inside @class UserRestImpl @method  createNewUser @param user :{}",user);
		try {
			User createdUser = userService.createNewUser(user);
			ResponseEntity<User> response = new ResponseEntity<User>(createdUser,HttpStatus.OK);
			return response;
		} catch (Exception e) {
			logger.error("Error occured inside @class UserRestImpl @method  createNewUser cause :{}",e);
			throw new RuntimeException(e.getMessage());
		}
	}
	
	@GetMapping("/getAll")
	public ResponseEntity<?> getAllUser(){
		logger.info("Inside @class UserRestImpl @method  getAllUser");
		try {
			List<User> list = userService.getAllUsers();
			return  new ResponseEntity<List<User>>(list,HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error occured inside @class UserRestImpl @method  getAllUser cause :{}",e);
			throw new RuntimeException(e.getMessage());
		}
	}
	
	@GetMapping("/getById/{userId}")
	public ResponseEntity<?> getUserById(@PathVariable Long userId){
		logger.info("Inside @class UserRestImpl @method  getUserById");
		try {
			User userDb =  userService.getUserById(userId);
			return  new ResponseEntity<User>(userDb,HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error occured inside @class UserRestImpl @method  getUserById cause :{}",e);
			throw new RuntimeException(e.getMessage());
		}
	}
	
	@PutMapping("/update")
	public ResponseEntity<User> updateExistingUser(@RequestBody User user){
		try {
			logger.info("Inside @class UserRestImpl @method  updateExistingUser");
			User UpdatedUser = userService.updateExistingUser(user);
			ResponseEntity<User> response = new ResponseEntity<User>(UpdatedUser,HttpStatus.OK);
			return response;
		} catch (Exception e) {
			logger.error("Error occured inside @class UserRestImpl @method  updateExistingUser cause :{}",e);
			throw new RuntimeException(e.getMessage());
		}
		
	}
	
	
	@DeleteMapping("/deleteById/{userId}")
	public ResponseEntity<String> deleteUserById (@PathVariable Long userId){
		try {
			logger.info("Inside @class UserRestImpl @method  deleteUserById");
			String str = userService.deleteExitingUser(userId);
			return new ResponseEntity<String>(str,HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error occured inside @class UserRestImpl @method  deleteUserById cause :{}",e);
			throw new RuntimeException(e.getMessage());
		}
	}
	
	

}
