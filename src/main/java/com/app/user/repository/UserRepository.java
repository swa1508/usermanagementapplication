package com.app.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.user.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
