package com.app.user.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.user.constant.UserStatus;
import com.app.user.model.User;
import com.app.user.repository.UserRepository;
import com.app.user.service.IUserService;

@Service
public class UserServiceImpl implements IUserService {
	
	private Logger logger = LogManager.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User createNewUser(User user) {
		logger.info("Inside @class UserServiceImpl @method  createNewUser @param user :{}",user);
		try {
			return userRepository.save(user);
		} catch (Exception e) {
			logger.error("Error occured inside @class UserServiceImpl @method  createNewUser cause :{}",e);
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public List<User> getAllUsers() {
		logger.info("Inside @class UserServiceImpl @method  getAllUsers ");
		try {
			return userRepository.findAll();
		} catch (Exception e) {
			logger.error("Error occured inside @class UserServiceImpl @method  getAllUsers cause :{}",e);
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Integer getallUsersCount() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUserById(Long userId) {
		logger.info("Inside @class UserServiceImpl @method  updateExistingUser @param userId :{} ",userId);		
		try {
			Optional<User> opt = userRepository.findById(userId);
			if(opt.isPresent()) {
				return opt.get();
			}
			throw new RuntimeException("User Not Found");
		} catch (Exception e) {
			logger.error("Error occured inside @class UserServiceImpl @method  updateExistingUser cause :{}",e);
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public User updateExistingUser(User user) {
		try {
			logger.info("Inside @class UserServiceImpl @method  updateExistingUser @param user :{} ",user);	
			User userDb = this.getUserById(user.getId());
			if(userDb != null) {
				return userRepository.save(user);
			}
			return null;
		} catch (Exception e) {
			logger.error("Error occured inside @class UserServiceImpl @method  updateExistingUser cause :{}",e);
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public String deleteExitingUser(Long userId) {
		try {
			logger.info("Inside @class UserServiceImpl @method  deleteExitingUser @param userId :{} ",userId);	
			User userDb = getUserById(userId);
			userDb.setUserStatus(UserStatus.DELETED);
			userRepository.save(userDb);
			return "User Deleted Successfully...";
		} catch (Exception e) {
			logger.error("Error occured inside @class UserServiceImpl @method  deleteExitingUser cause :{}",e);
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public String deleteAllUser() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
