package com.app.user.service;

import java.util.List;

import com.app.user.model.User;

public interface IUserService {
	
	List<User> getAllUsers();
	Integer getallUsersCount();
	User getUserById(Long userId);
	User createNewUser(User user);
	User updateExistingUser(User user);
	String deleteExitingUser(Long userId);
	String deleteAllUser();

}
