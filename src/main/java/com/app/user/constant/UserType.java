package com.app.user.constant;

public enum UserType {
	ADMIN,
	GUEST,
	STUDENT,
	TEACHER,
	EMPLOYEE
}
