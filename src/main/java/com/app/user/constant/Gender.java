package com.app.user.constant;

public enum Gender {
	MALE,
	FEMALE,
	OTHERS
}
