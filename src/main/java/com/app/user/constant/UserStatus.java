package com.app.user.constant;

public enum UserStatus {
	NEW,
	ACTIVE,
	INACTIVE,
	DELETED,
}
